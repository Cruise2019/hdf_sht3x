#ifndef _SHT3X_IF_H_
#define _SHT3X_IF_H_

#include <stdint.h>

#include "sht3x.h"

typedef void* DevHandle;

#define SHT3X_DEV_SERVICE_NAME_PREFIX   "sht3x_%d"
#define MAX_DEV_NAME_SIZE 32

// 打开sht3x设备
DevHandle sht3xOpen(uint32_t num);

// 关闭sht3x设备
void sht3xClose(DevHandle dev);

// 设置设备的工作模式
int32_t sht3xSetMode(DevHandle dev, uint8_t mode);

// 使能设备
int32_t sht3xEnable(DevHandle dev);

// 失能设备
int32_t sht3xDisable(DevHandle dev);

// 读取温湿度数据
int32_t sht3xGetData(DevHandle dev, sht3x_data *data);

// 获取温度数据
int32_t sht3xGetTempData(DevHandle dev, float *temperature);

// 获取湿度数据
int32_t sht3xGetHumiData(DevHandle dev, float *humidity);

// 设置采样间隔
int32_t sht3xSetSamplingInterval(DevHandle dev, int64_t interval);

#endif // !_SHT3X_IF_H_
