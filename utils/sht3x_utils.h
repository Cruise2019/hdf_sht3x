#ifndef _SHT3X_UTILS_H_
#define _SHT3X_UTILS_H_

#include "stdint.h"

// CRC校验相关
uint8_t sht3xUtilsCalcCrc(uint8_t *data, uint8_t len);
int32_t sht3xUtilsVerifyCrc(uint8_t *data, uint8_t len, uint8_t checksum);

// 计算相关
float sht3xUtilsCalcTemperature(uint16_t rawValue);
float sht3xUtilsCalcHumidity(uint16_t rawValue);
uint16_t sht3xUtilsCalcRawTemperature(float temperature);
uint16_t sht3xUtilsCalcRawHumidity(float humidity);

#endif

