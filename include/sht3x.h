#ifndef _SHT_3X_H_
#define _SHT_3X_H_

#include "stdio.h"
#include "stdint.h"

// 温湿度数据
typedef struct _sht3x_data {
    float temperature;
    float humidity;
} sht3x_data;

// HDF CMD
#define SHT3X_CMD_SET_MODE                  (1)
#define SHT3X_CMD_ENABLE                    (2)
#define SHT3X_CMD_DISABLE                   (3)
#define SHT3X_CMD_SET_SAMPLING_INTERVAL     (4)
#define SHT3X_CMD_SOFT_RESET                (5)

#define SHT3X_CMD_GET_TEMP_HUMI             (15)
#define SHT3X_CMD_GET_TEMP                  (16)
#define SHT3X_CMD_GET_HUMI                  (17)

/*
| 7 | 6    | 5 | 4       | 3 | 2 | 1 | 0 |
| repeatab | mesure mode | - | freq      | 

repeatab:
    ||
    0100 0000   HIGH
    1000 0000   MEDIUM
    1100 0000   LOW

mesure mode:
      ||
    0001 0000   PERIOD
    0010 0000   POLL
    0011 0000   CLK STRETCH

freq(HZ):
          |||
    0000 0001   0.5   
    0000 0010   1
    0000 0011   2
    0000 0100   4
    0000 0101   10
*/

// 重复度
// @Repeatab
#define SHT3X_REPEATAB_HIGH                 (0x01)
#define SHT3X_REPEATAB_MEDIUM               (0x02)
#define SHT3X_REPEATAB_LOW                  (0x03)

// 测量模式
// @MeasureMode
#define SHT3X_PERIOD                        (0x01)
#define SHT3X_POLL                          (0x02)
#define SHT3X_CLK_STRETCH                   (0x03)

// 周期
// @Frequency
#define SHT3X_FREQUENCY_HZ5                 (0x01)
#define SHT3X_FREQUENCY_1HZ                 (0x02)
#define SHT3X_FREQUENCY_2HZ                 (0x03)
#define SHT3X_FREQUENCY_4HZ                 (0x04)
#define SHT3X_FREQUENCY_10HZ                (0x05)

#define SHT3X_REPEATAB_MASK(repeatab)       ( ((repeatab) & 0x3 ) << 6 )
#define SHT3X_MEASURE_MODE_MASK(mode)       ( ((mode)     & 0x3 ) << 4 )
#define SHT3X_FREQ_MASK(freq)               ( ((freq)     & 0x7 ) << 0 )

#define SHT3X_MODE_FORMATE(repeatab, mode, freq)    (SHT3X_REPEATAB_MASK(repeatab) | SHT3X_MEASURE_MODE_MASK(mode) | SHT3X_FREQ_MASK(freq))

// 测量模式
// @WorkMode
#define SHT3X_MODE_NONE                     (0)

#define SHT3X_MODE_POLL_H                   SHT3X_MODE_FORMATE(SHT3X_REPEATAB_HIGH, SHT3X_POLL, SHT3X_MODE_NONE) 
#define SHT3X_MODE_POLL_M                   SHT3X_MODE_FORMATE(SHT3X_REPEATAB_MEDIUM, SHT3X_POLL, SHT3X_MODE_NONE) 
#define SHT3X_MODE_POLL_L                   SHT3X_MODE_FORMATE(SHT3X_REPEATAB_LOW, SHT3X_POLL, SHT3X_MODE_NONE) 

#define SHT3X_MODE_CLK_STRETCH_H            SHT3X_MODE_FORMATE(SHT3X_REPEATAB_HIGH, SHT3X_CLK_STRETCH, SHT3X_MODE_NONE)
#define SHT3X_MODE_CLK_STRETCH_M            SHT3X_MODE_FORMATE(SHT3X_REPEATAB_MEDIUM, SHT3X_CLK_STRETCH, SHT3X_MODE_NONE)
#define SHT3X_MODE_CLK_STRETCH_L            SHT3X_MODE_FORMATE(SHT3X_REPEATAB_LOW, SHT3X_CLK_STRETCH, SHT3X_MODE_NONE)

#define SHT3X_MODE_PERIOD_HZ5_H             SHT3X_MODE_FORMATE(SHT3X_REPEATAB_HIGH, SHT3X_PERIOD, SHT3X_FREQUENCY_HZ5)
#define SHT3X_MODE_PERIOD_HZ5_M             SHT3X_MODE_FORMATE(SHT3X_REPEATAB_MEDIUM, SHT3X_PERIOD, SHT3X_FREQUENCY_HZ5)
#define SHT3X_MODE_PERIOD_HZ5_L             SHT3X_MODE_FORMATE(SHT3X_REPEATAB_LOW, SHT3X_PERIOD, SHT3X_FREQUENCY_HZ5) 

#define SHT3X_MODE_PERIOD_1HZ_H             SHT3X_MODE_FORMATE(SHT3X_REPEATAB_HIGH, SHT3X_PERIOD, SHT3X_FREQUENCY_1HZ) 
#define SHT3X_MODE_PERIOD_1HZ_M             SHT3X_MODE_FORMATE(SHT3X_REPEATAB_MEDIUM, SHT3X_PERIOD, SHT3X_FREQUENCY_1HZ) 
#define SHT3X_MODE_PERIOD_1HZ_L             SHT3X_MODE_FORMATE(SHT3X_REPEATAB_LOW, SHT3X_PERIOD, SHT3X_FREQUENCY_1HZ) 

#define SHT3X_MODE_PERIOD_2HZ_H             SHT3X_MODE_FORMATE(SHT3X_REPEATAB_HIGH, SHT3X_PERIOD, SHT3X_FREQUENCY_2HZ) 
#define SHT3X_MODE_PERIOD_2HZ_M             SHT3X_MODE_FORMATE(SHT3X_REPEATAB_MEDIUM, SHT3X_PERIOD, SHT3X_FREQUENCY_2HZ) 
#define SHT3X_MODE_PERIOD_2HZ_L             SHT3X_MODE_FORMATE(SHT3X_REPEATAB_LOW, SHT3X_PERIOD, SHT3X_FREQUENCY_2HZ) 

#define SHT3X_MODE_PERIOD_4HZ_H             SHT3X_MODE_FORMATE(SHT3X_REPEATAB_HIGH, SHT3X_PERIOD, SHT3X_FREQUENCY_4HZ) 
#define SHT3X_MODE_PERIOD_4HZ_M             SHT3X_MODE_FORMATE(SHT3X_REPEATAB_MEDIUM, SHT3X_PERIOD, SHT3X_FREQUENCY_4HZ) 
#define SHT3X_MODE_PERIOD_4HZ_L             SHT3X_MODE_FORMATE(SHT3X_REPEATAB_LOW, SHT3X_PERIOD, SHT3X_FREQUENCY_4HZ) 

#define SHT3X_MODE_PERIOD_10HZ_H            SHT3X_MODE_FORMATE(SHT3X_REPEATAB_HIGH, SHT3X_PERIOD, SHT3X_FREQUENCY_10HZ) 
#define SHT3X_MODE_PERIOD_10HZ_M            SHT3X_MODE_FORMATE(SHT3X_REPEATAB_MEDIUM, SHT3X_PERIOD, SHT3X_FREQUENCY_10HZ) 
#define SHT3X_MODE_PERIOD_10HZ_L            SHT3X_MODE_FORMATE(SHT3X_REPEATAB_LOW, SHT3X_PERIOD, SHT3X_FREQUENCY_10HZ) 

#endif

