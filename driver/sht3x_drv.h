#ifndef _SHT3X_DRV_H_
#define _SHT3X_DRV_H_

#include <stdint.h>

// sht3x
#include "sht3x.h"

// osal
#include "osal.h"

#include "hdf_platform.h"

typedef struct {
    uint8_t adVal;              // ad引脚电平
    uint16_t i2cPortNum;        // i2c总线端口号
} sht3xDrvCfg;

//////////////////////////////////////////////////
// 参数设置相关
//////////////////////////////////////////////////

// 获取 / 设置 采样频率
extern int32_t sht3xDrvSetSamplingInterval(DevHandle dev, int64_t interval);
extern int64_t sht3xDrvGetSamplingInterval(DevHandle dev);

//////////////////////////////////////////////////
// 操作相关
//////////////////////////////////////////////////

extern DevHandle sht3xDrvOpen(sht3xDrvCfg *cfg);
extern void sht3xDrvClose(DevHandle dev);

// 设置温湿度测量模式
extern int32_t sht3xDrvSetMode(DevHandle dev, uint8_t mode);

// enable / disable
extern int32_t sht3xDrvEnable(DevHandle dev);
extern int32_t sht3xDrvDisable(DevHandle dev);

// 读取温湿度数据
extern int32_t sht3xDrvReadData(DevHandle dev);

// 获取温湿度数据
extern int32_t sht3xDrvGetTemperature(DevHandle dev, float *temperature);
extern int32_t sht3xDrvGetHumidity(DevHandle dev, float *humidity);

// 软复位
extern int32_t sht3xDrvSoftReset(DevHandle dev);

#endif
