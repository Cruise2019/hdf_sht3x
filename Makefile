# 统一的编译配置文件
include $(LITEOSTOPDIR)/config.mk
include $(LITEOSTOPDIR)/../../drivers/adapter/khdf/liteos/lite.mk

# 驱动模块的名字
MODULE_NAME := sht3x

# 编译参数
LOCAL_CFLAGS += $(HDF_INCLUDE)
LOCAL_CFLAGS += -fstack-protector-strong

# 驱动模块的头文件目录
LOCAL_INCLUDE += 	$(LITEOSTOPDIR)/drivers/framework/model/sensor/driver/common/include \
					$(LITEOSTOPDIR)/drivers/framework/model/sensor/driver/include \
					./include \
					./utils \
					./drivers 

# 驱动模块的源文件
LOCAL_SRCS += ./driver/sht3x.c \
				./utils/sht3x_utils.c \
				./driver/sht3x_drv.c

# 统一的编译脚本
include $(HDF_DRIVER)
