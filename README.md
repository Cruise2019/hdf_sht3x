# 基于 OpenHarmonyOS HDF驱动框架的sht3x温湿度传感器驱动  
---

## 源码路径:  
> vendor/Sensirion/hdf/sht3x  

## 驱动加入编译:
> device/hisilicon/drivers/lite.mk  

在末尾添加:
```
# sht3x
LITEOS_BASELIB += -lsht3x  
LIB_SUBDIRS += $(LITEOSTOPDIR)/../../vendor/Sensirion/hdf/sht3x  
```

这一步会将驱动源码加入编译

## 配置HCS节点：


### 添加设备节点:
> vendor/hisilicon/hispark_taurus/config/device_info     
```
sht3x_host :: host {
    hostName = "sht3x_host";            // host 名称
    priority = 101;                     // 加载优先级

    sht3x_device :: device {            // 设备
        sht3x :: deviceNode {           // 设备节点
            policy = 2;                 // 驱动发布策略(是否对外提供服务...)
            priority = 101;             // 设备加载优先级
            preload = 0;                // 直接加载还是按需加载
            permission = 0664;          // 节点权限
            moduleName = "sht3x";           // 驱动名称，该字段的值必须和驱动入口结构的moduleName值一致
            serviceName = "sht3x_0";    // 对外发布服务的名称
            deviceMatchAttr = "hdf_sht3x_0";// 驱动私有属性匹配字段
        }
    }
}
```

### 配置驱动私有属性:
> sht3x/config/sht3x.hcs  

```
root {
    sht3x_config {
        sht3x_device_0 {
            match_attr = "hdf_sht3x_0"; // 驱动私有属性匹配字段  
            port = 6;                   // 使用 i2c port 6  
            addr = 0;                   // addr 低电平(0):0x44 高电平(1):0x45  
        }
    }
}
```

### 添加驱动私有属性节点
将私有属性文件添加到顶层hcs文件中
> vendor/hisilicon/hispark_taurus/config/hdf.hcs  

```
// 添加该字段  
#include "../../../Sensirion/hdf/sht3x/config/sht3x.hcs"  
```

驱动源码加入编译后,会根据hcs的节点配置生成对应的驱动节点  

## 用于测试的C程序加入编译:  
> build/lite/components/applications.json  

添加节点:
```
{
    "component": "sht3x_app",
    "description": "sht3x app",
    "optional": "true",
    "dirs": [
        "vendor/Sensirion/hdf/sht3x"
    ],
    "targets": [
        "//vendor/Sensirion/hdf/sht3x:sht3x_app"
    ],
    "rom": "",
    "ram": "",
    "output": [
        "sht3x_app"
    ],
    "adapted_kernel": [ "liteos_a" ],
    "features": [],
    "deps": {
        "third_party": [],
        "kernel_special": {},
        "board_special": {},
        "components": [
        "adapter_uhdf"
        ]
    }
}
```

## 加入单板编译:

> vendor/hisilicon/hispark_taurus/config.json

```
{
    "subsystem": "applications",
    "components": [
        { "component": "camera_sample_app", "features":[] },
        { "component": "camera_screensaver_app", "features":[] },

        // 添加该节点
        { "component": "sht3x_app", "features":[]  }
    ]
},
```

在 /bin 目录下生成测试app

## HDF驱动设备节点生成路径:

> /dev/hdf/xxx

设备烧录,启动后检查路径下是否有生成对应的节点

## 驱动测试app  

路径：
> /bin/sht3x_app

使用方式:
> ./sht3x_app  -M 1 -T 10 -P 1000   
> -M [ 1|2|3|4|5 ]  
> -T times  
> -P period  
> 例: ./sht3x_app -M 1 -T 10 -P 1000   

## 后续开发计划:

- [x] 支持HCS配置  
- [ ] 支持HDF sensor框架  
- [ ] ~~源码迁移到driver目录下~~ (暂时先不考虑)
- [x] 优化app,实现HDI     
- [x] 支持 poll/clk_stretch 单次测量模式
- [ ] 支持alert  
...  

---

